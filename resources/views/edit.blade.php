<form action="{{ url('update', $edit->id) }}" method="post">
    {{ csrf_field() }}
    <table border="5px" class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <tr>
            <th>Nama :</th>
            <td>
                <input type="text" name="nama" id="nama" value="{{ $edit->nama }}">
            </td>
        </tr>
        <tr>
            <th>Jurusan :</th>
            <td>
                <input type="text" name="jurusan" id="jurusan" value="{{ $edit->jurusan }}">
            </td>
        </tr>
        <tr>
            <th>Fakultas :</th>
            <td>
                <input type="text" name="fakultas" id="fakultas" value="{{ $edit->fakultas }}">
            </td>
        </tr>
        <tr>
            <th>Semester :</th>
            <td>
                <input type="text" name="semester" id="semester" value="{{ $edit->semester }}">
            </td>
        </tr>
        <tr>
            <td>
                <a href="/data">Kembali</a>
            </td>
            <td>
                <button>EDIT</button>
            </td>
        </tr>
    </table>
</form>