@extends('index')

@section('content')

<section>
    <div class="container">
      
      <div class="docs-content">
        <h2 class="entry-title"> Getting Started</h2>
        <h3 id="welcome"> Template Info</h3>
        <img src="assets/img/Pilvia.png" style="border: 1px solid #ddd;text-align: center;margin: 0 auto;box-shadow: 0px 5px 8px #ddd;"/>
        <p>Thank You For Downloading Our Template. Please Read The Documentation Carefully, If You Still Have Any Problem Please Feel Free To Email Via Our
        <a href="mailto:templateclue@gmail.com" target="_blank"><b>Email Address</b></a>.</p>
        <p class="note">Note : Premium And 6 Months Support Only Provided To Full Version Buyers</p>
        <p><b>It Is Restricted To Remove Our Credit Link Due To The Erasure Of The Credit Links Will Make Your Site To Redirect <a href="https://www.templateclue.com/" target="new">TemplateClue</a> For Those Who Want To Remove Can Purchase The Template.</b></p>
        <h3>What You Get?</h3>
        <ul>
          <li>Removable Footer Credits</li>
          <li>6 Months Support</li>
              <li>Full Access</li>
          <li>Lifetime Template Updates</li>
          <li>And Much More...</li>
        </ul>
        <p><a href="http://www.templateclue.com" target="_blank" class="purchase_now">Purchase Now!!!</a></p>
        <h3 id="features">Features</h3>
        <ul>
        
          <li>Responsive</li>
          <li>SEO Friendly</li>
          <li>Label Post Widget</li>
          <li>Clean Look</li>
          <li>Homepage Share Button</li>
          <li>Responsive Menu</li>
          <li>Related Posts with Thumb</li>
          <li>Clean Looking Buttons</li>
          <li>Social Share Button</li>
          <li>Load More Page Navigation</li>
          <li>Custom Recent Comment Widget</li>
          <li>Multi Author Box</li>
          <li>Clean Comment Style</li>
          <li>And more..</li>
       
        </ul>
       
        <h3 id="installation">Installation</h3>
        <p> You Can Install The Template In Two Ways:</p>
        <ol>
        <li><b>Uploading</b>: Go to your dashboard > Template > Backup / Restore > Upload the .xml.</li>
        <li><b>Direct Uploading</b>: By Copy And Paste.Just Copy Your Blogger Template By Opening It In Notepad Select The Whole Code And Paste It Into 
        <a href="https://www.blogger.com/" target="_blank">Template Editor</a>.</li>
        </ol>
        <p class="note" style="background:#34495e;">1. Uploading Method</p>
        <ul style="list-style:none;">
        <li><img src="assets/img/1.png"></li>
        <li><img src="assets/img/2.png"></li>
        </ul>
        
        <p class="note" style="background:#34495e;">2. License Activation Method</p>
        <p>Go to your Blogger Account - Your Blog & select Layout</p>
        <p>Now In License Widget Click On Edit</p>
        <p>And Enter Your Product Id And License Key At The Place Of Title And Content Input Field.</p>
        <ul style="list-style:none;">
        <li><img src="assets/img/step1.png"></li>
        <li><img src="assets/img/step2.png"></li>
        </ul>
        <p><strong>Watch Video Full Video Blogger Template Using License Key: </strong> <a href="https://www.youtube.com/watch?v=U80IYw6PieM" title="Activate Blogger Template License" target="_blank">Click Here</a></p>
        
        <h2 class="entry-title">Customize Template</h2>
        <h3 id="Top">Top Area</h3>
        <span class="sub_heading">Header  Background</span>
        <p style="overflow:auto;">Go to Blogger.com -> Your Blog -> Template -> Edit HTML And Find ( CTRL + F ) This:- &lt;!-- Header Background --&gt;<br>
        Now Change Header Background With Yours</p>
        
        <span class="sub_heading">Header Text</span>
        <p>Go to Blogger.com -> Your Blog -> Template -> Edit HTML And Find ( CTRL + F ) This:- &lt;!-- ====== Header Text======  --&gt;</p>
        <p>Now Change Slides Text With Yours</p>
        
        <span class="sub_heading">Menu Navigation Items </span>
        <p>Go to Blogger.com -> Your Blog -> Template -> Edit HTML And Find ( CTRL + F ) This:- &lt;!-- Top Menu --&gt;</p>
        <p>Now Change It With Yours</p>
      
        <hr>
        <h3 id="header">Header Area</h3>
        <p><span class="sub_heading">Logo Setting:</span>
          </p>
        <p><img src="assets/ss/1.png"/>
          </p>
        <p>Go to your Blogger Account - Your Blog & select Layout</p>
        <p>Now In Header Widget Click On Edit</p>
        <p>Upload Your Logo And Select <b>"Instead of title and description"</b> as the "Placement" option & Select Shrink To Fit.</p>
        <img src="assets/img/settings.png"/>
        
        <hr>
        
      <h3 id="label-post">About</h3> 
          <p>Go to Blogger.com -> Your Blog -> Template -> Edit HTML And One By One Find ( CTRL + F ) All This:-</p>
          <ol>
          <li>&lt;!-- About Section Heading --&gt; (Add Your Title)</li>
          <li>&lt;!-- About Section Sub Heading --&gt; (Add Your Sub Heading)</li>
          <li>&lt;!-- About Section Content --&gt; (Add Your Content)</li>
          <li>&lt;!-- About Section Social Button --&gt; (Add Your Link)</li>
          </ol>
        <hr>
          
       <h3 id="footer">Sections</h3>
       <p>Go to Blogger.com -> Your Blog -> Template -> Edit HTML And One By One Find ( CTRL + F ) All This:-</p>
          <ol>
          <li>Find :- &lt;!-- Our Skills Section --&gt;</li>
          <ol>
          <li>[In Skills Section] Change The</li>
          <li>The Title (Find &lt;!-- Our Skills Title --&gt;)</li>
          <li>Label (Find &lt;!-- Our Skills Label --&gt;)</li>
          </ol>
          <li>Find :- &lt;!-- Services Section --&gt;</li>
          <ol>
            <li>[In Services Section] Change The:-</li>
            <li>Icon (Find &lt;!-- Services Section Icon --&gt;)</li>
            <li>Title (Find &lt;!-- Services Section Title --&gt;)</li>
            <li>Content (Find &lt;!-- Services Section Content --&gt;)</li>
          </ol>
          <li>Find :- &lt;!-- Contact Us Section --&gt; (Note:- The Contact Form Work Automatically)</li>
          <ol>
            <li>[In CONTACT Section] Change The:-</li>
            <li>Details (Find &lt;!-- Contact Section Details --&gt;)</li>
          </ol>
          <li>Find :- &lt;!-- Footer --&gt;</li>
          <ol>
          <li>[In Footer Section] Change The:-</li>
          <li>Social Links (Find &lt;!-- Footer Social Links--&gt;)</li>
          </ol>
          </ol>
          <hr>
      <h3 id="settings">Settings</h3>
       <span class="sub_heading">Mobile Version</span>
       <p>To Use Responsive Design In Mobile Devices Than First You Need To Enable It.</p>
       <p><b>Note:</b>The Responsive Menu Link Sub Menu Will Not Work(Open) In The Desktop If You Resize Your Window It Is Only Work On Mobile Devices.</p>
         <ol>
          <li>Go to Blogger.com -> Your Blog -> Template -> Now In Mobile Section Click On Gear Button As Shown In Given Below Image</li>
        </ol>
       <img src="assets/img/3.png">
       <img src="assets/img/4.png">
      <p>2. Click On "No. Show desktop template on mobile devices." And Than Click On Save.</p>
      <span class="sub_heading">Change The Post Date Format:</span>
      <p>Go to Blogger Dashboard -> Settings -> Language and Formatting -> Formatting -> Timestamp Format -> Set It To Date Instead Of Time.<br>Now Click On Save...</p>
      <img src="assets/img/5.png">
       </div>
    </div>
  </section>
  
  <section class="vibrant centered">
    <div class="">
      <h4>We Will Setup Template For You In The Proper And Clean Way <a href="http://www.templateclue.com/" target="_blank" class="b_i">Buy Theme + Installation Service</a></h4>
    </div>
  </section>

@endsection

