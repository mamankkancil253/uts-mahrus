﻿<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>Pilvia | Documentation</title>
<link href="http://fonts.googleapis.com/css?family=Raleway:700,300" rel="stylesheet"
        type="text/css">
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/css/prettify.css">
<style>
header , .pull-right a , .docs-nav a:hover, .docs-nav .active, span.sub_heading {background:#222;}
a,nav a:active,a:active{color:#222;}
b{font-size: 13px;}
body{overflow-x: hidden;}
</style>
</head>
<body>
<!-- <div class="wrapper"> -->

@extends('header')

@yield('content')

@extends('futer')

<!-- </div> -->


</body>
<script src="assets/js/jquery.min.js"></script> 
 
<script type="text/javascript" src="assets/js/prettify/prettify.js"></script> 
<script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css&skin=sunburst"></script>
<script src="assets/js/layout.js"></script>
 <script src="assets/js/jquery.localscroll-1.2.7.js" type="text/javascript"></script>
 <script src="assets/js/jquery.scrollTo-1.4.3.1.js" type="text/javascript"></script>
</html>

