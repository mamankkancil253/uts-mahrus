<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    @foreach ($cari as $item)
        <tr>
            <th>ID : </th>
            <td>{{ $item->id }}</td>
        </tr>
        <tr>
            <th>Nama : </th>
            <td>{{ $item->nama }}</td>
        </tr>
        <tr>
            <th>Jurusan : </th>
            <td>{{ $item->jurusan }}</td>
        </tr>
        <tr>
            <th>Fakultas : </th>
            <td>{{ $item->fakultas }}</td>
        </tr>
        <tr>
            <th>Semester : </th>
            <td>{{ $item->semester }}</td>
        </tr>
        <tr>
            <th>Option</th>
            <td>
                <a href="/data">Kembali</a>
            </td>
        </tr>
    @endforeach
</table>