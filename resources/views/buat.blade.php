<form action="simpan" method="post">
    {{ csrf_field() }}
    <table border="5px" class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <tr>
            <th>Nama :</th>
            <td>
                <input type="text" name="nama" id="nama">
            </td>
        </tr>
        <tr>
            <th>Jurusan : </th>
            <td>
                <input type="text" name="jurusan" id="jurusan">
            </td>
        </tr>
        <tr>
            <th>Fakultas : </th>
            <td>
                <input type="text" name="fakultas" id="fakultas">
            </td>
        </tr>
        <tr>
            <th>Semester : </th>
            <td>
                <input type="text" name="semester" id="semester">
            </td>
        </tr>
        <tr>
            <td>
                <a href="/data">Kembali</a>
            </td>
            <td>
                <button>BUAT</button>
            </td>
        </tr>
    </table>
</form>
