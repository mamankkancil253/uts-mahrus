<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Mahasiswa;

class MahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Mahasiswa::create([
            'nama' => 'Mahrus',
            'jurusan' => 'Teknik Informatika',
            'fakultas' => 'Teknik',
            'semester' => '3',
        ]);
        Mahasiswa::create([
            'nama' => 'Dinda',
            'jurusan' => 'Kebidanan',
            'fakultas' => 'Kebidanan',
            'semester' => '5',
        ]);
    }
}
